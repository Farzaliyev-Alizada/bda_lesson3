import java.util.Scanner;

/*
 * 22) Scanner-dən istifadə edərək calculator appi yazın
 * */
public class Task22 {

    public static void main(String[] args) {
        double num1, num2;
        Scanner input = new Scanner(System.in);
        char operator;
        double result = 0;

        // Settings menu
        String colorScheme = "default";
        int fontSize = 12;
        boolean isDarkMode = false;

        System.out.println("==== AYARLAR MENYUSU ====");
        System.out.println("1. Rəngi dəyiş");
        System.out.println("2. Şrift ölçüsünü dəyiş ");
        System.out.println("3. Dark mode aktivləşir");
        System.out.println("4. ÇIXIŞ");

        boolean exitSettings = false;
        while (!exitSettings) {
            System.out.print("Seçiminizi edin: ");
            int option = input.nextInt();
            switch (option) {
                case 1:
                    System.out.print("Rengi deyish (default, light, dark): ");
                    colorScheme = input.next();
                    break;
                case 2:
                    System.out.print("Shrift olchusunu deyish (e.g. 12): ");
                    fontSize = input.nextInt();
                    break;
                case 3:
                    isDarkMode = true;
                    break;
                case 4:
                    exitSettings = true;
                    break;
                default:
                    System.out.println("Yalnış dəyər");
            }
        }

        // Calculator
        while (true) {
            System.out.print("İlk ədədi daxil edin: ");
            num1 = input.nextDouble();
            System.out.print(" Operatoru seçin (+, -, *, /, %, ^): ");
            operator = input.next().charAt(0);
            System.out.print("İkinci ədədi daxil edin: ");
            num2 = input.nextDouble();

            switch (operator) {
                case '+':
                    result = num1 + num2;
                    break;
                case '-':
                    result = num1 - num2;
                    break;
                case '*':
                    result = num1 * num2;
                    break;
                case '/':
                    if (num2 == 0) {
                        throw new ArithmeticException("Sıfra bölmək olmaz");
                    }
                    result = num1 / num2;
                    break;
                case '%':
                    result = num1 % num2;
                    break;
                case '^':
                    result = Math.pow(num1, num2);
                    break;
                default:
                    System.out.println("Yalnış operator daxil edildi");
                    continue;
            }

            String operatorStr = String.valueOf(operator);
            String output = String.format("%.2f %s %.2f = %.2f", num1, operatorStr, num2, result);

            if (colorScheme.equals("light")) {
                System.out.println("\u001B[37m" + output + "\u001B[0m");
            } else if (colorScheme.equals("dark")) {
                System.out.println("\u001B[30m" + output + "\u001B[0m");
            } else {
                System.out.println(output);
            }

            // Change font size
            System.out.println("\033[" + fontSize + "m");

            System.out.print("Hesablamanı davam etdirmək istəyirsiz? (Y/N): ");
            String choice = input.next();
            if (!choice.equalsIgnoreCase("y")) {
                break;
            }
        }

        // Exit message
        if (isDarkMode) {
            System.out.println("\u001B[37mKalkulyator proqramını istifadə etdiyiniz üçün təşəkkürlər!Hələlik!\u001B[0m");
        } else {
            System.out.println("Kalkulyator proqramını istifadə etdiyiniz üçün təşəkkürlər!Hələlik!");
        }
    }
}
