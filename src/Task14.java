import java.util.Scanner;

/*
* 14) Iki ferqli tip gotrun ve onlari vurun alinan cavab 100u kechse Sout("100u kechdi yazisi chixardin"0;
* */
public class Task14 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Tam eded daxil edin: ");
        int num1 = input.nextInt();

        System.out.println("Kesr eded daxil edin: ");
        double num2 = input.nextDouble();

        double result = num1*num2;

        if (result > 100) {
            System.out.print(String.format("%.1f", result)+" Netice 100-u kechdi");
        } else {
            System.out.println(String.format("%.1f", result));
        }
    }

}
