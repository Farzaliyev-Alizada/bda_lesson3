/*
* 33.method String a, int begin, int end daxil edilir. A-nin begin ve end arasini alt alta chap edir.
(Hello World, 3,7) netice olacaq:
l
o
W

* */
import java.util.Scanner;

public class Task33 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("String deyerini daxil edin:");
        String str = input.nextLine();

        System.out.println("begin deyerini daxil edin: ");
        int begin = input.nextInt();

        System.out.println("end deyerini daxil edin: ");
        int end = input.nextInt();


        String result = printSubstring(str, begin, end);
        System.out.println(result);
    }

    public static String printSubstring(String a, int begin, int end) {
        for (int i = begin; i <= end; i++) {
            System.out.println(a.charAt(i));
        }
        return a;
    }

}
