import java.util.Scanner;

/*
* 31.Method String s, char c, int count qebul edir. S-i count qeder c ile birleshdirir ve geriye return edir
foo(String s, char c, int count){}
foo(“Soz”, ‘c’, 5); -> Sozccccc

* */
public class Task31 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("string deyerini daxil edin:");
        String str= input.nextLine();

        System.out.println("char deyerini daxil edin: ");
        String str2 = input.next();
        char ch = str2.charAt(0);

        System.out.println("Count deyerini daxil edin: ");
        int count = input.nextInt();

        String result = stringMaker(str,ch,count);
        System.out.println(result);


    }
    public static String stringMaker (String s, char c, int count) {
        StringBuilder newString = new StringBuilder(s);
        for (int i = 0; i < count; i++) {
            newString.append(c);
        }
        return newString.toString();
    }

}
