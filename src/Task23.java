import java.util.Scanner;

/*
* 23)methoda daxil edilen reqemin musbet, menfi yoxsa 0 oldugunu teyin eden method yazin.
* */
public class Task23 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Reqem daxil edin: ");
        int number = input.nextInt();
        checkNumber(number);
    }
    public static int checkNumber(int num) {
        if (num > 0) {
            System.out.println("Positive");
        } else if (num < 0) {
            System.out.println("Negative");
        } else {
            System.out.println("Zero");
        }
        return num;
    }

}
