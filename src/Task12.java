import java.util.Scanner;

/*
* 12) Verilmish ededin menfi ya musbet olmagini tapin
* */
public class Task12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Reqem daxil edin: ");
        int number = input.nextInt();

        if(number>0){
            System.out.println(number + " musbet ededdir");
        } else {
            System.out.println(number + " menfi ededdir");
        }
    }
}
