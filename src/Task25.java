import java.util.Scanner;

/*
* 25)Scanner ile mushteriden 3 reqem isteyin ve en boyuk reqem hansidirsa onu chap edin
* */
public class Task25 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num1,num2,num3,largest;
        System.out.println("Birinci reqemi daxil edin: ");
         num1=input.nextInt();
        System.out.println("Ikinci reqemi daxil edin: ");
         num2=input.nextInt();
        System.out.println("Uchuncu reqemi daxil edin: ");
         num3=input.nextInt();

        if (num1 >= num2 && num1 >= num3) {
            largest = num1;
        } else if (num2 >= num1 && num2 >= num3) {
            largest = num2;
        } else {
            largest = num3;
        }

        System.out.println("En boyuk eded: " + largest);


    }
}
