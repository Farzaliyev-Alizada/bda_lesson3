import java.util.Scanner;

/*
* 6) Boolean tip daxil edin eyer true olsa bu ededleri bir birine vurun(*) yox eyer false olsa bu ededleri toplayin
* */
public class Task6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter first number: ");
        int num1= input.nextInt();
        System.out.println("Please enter second number: ");
        int num2 = input.nextInt();

        System.out.println("Please enter a boolean value (true/false): ");
        boolean booleanValue = input.nextBoolean();

        if (booleanValue){
            int result = num1*num2;
            System.out.println(num1 +" * "+ num2+ " = " + result);
        }
        else {
            int result = num1+num2;
            System.out.println(num1 +" + "+ num2+ " = " + result);
        }

    }
}
