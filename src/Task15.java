import java.util.Scanner;

/*
* 15)Scannerden iki eded alin ve +,-,*,/, bu sim vollardan hansi gelse o emelyati aparsin(kalkulator);
* */
import java.util.Scanner;
public class Task15 {
        public static void main(String[] args) {
            int num1, num2;
            Scanner input = new Scanner(System.in);
            System.out.println("Please enter first number: ");
            num1 = input.nextInt();
            System.out.println("Please enter operator(+,-,*,/): ");
            char operator = input.next().charAt(0);
            System.out.println("Please enter second number: ");
            num2 = input.nextInt();
            int result = 0;
            switch (operator) {
                case '+':
                    result = num1 + num2;
                    break;
                case '-':
                    result = num1 - num2;
                    break;
                case '*':
                    result = num1 * num2;
                    break;
                case '/':
                    if (num2 == 0) {
                        throw new ArithmeticException("You cannot divide by zero");
                    }
                    result = num1 / num2;
                    break;
                default:
                    System.out.println("You have entered wrong operator!");
            }
            System.out.printf("%d %c %d = %d", num1, operator, num2, result);

        }
    }

