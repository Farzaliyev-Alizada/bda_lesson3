import java.util.Scanner;

/*
* 10) Switch Case-den istf ederek Heftenin gunlerini ve aylari chixartmaq.
* */
public class Task10 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int dayOfWeek;
        System.out.println("Həftənin gününü daxil edin: ");
        dayOfWeek=input.nextInt();
        int numberOfMonth;
        System.out.println("Ayın nömrəsini daxil edin: ");
        numberOfMonth=input.nextInt();
        switch (dayOfWeek){
            case 1:
                System.out.println("Bazar etəsi");
                break;
            case 2:
                System.out.println("Çərşənbə axşamı");
                break;
            case 3:
                System.out.println("Çərşənbə");
                break;
            case 4:
                System.out.println("Cümə axşamı");
                break;
            case 5:
                System.out.println("Cümə");
                break;
            case 6:
                System.out.println("Şənbə");
                break;
            case 7:
                System.out.println("Bazar");
                break;
            default:
                System.out.println("Yalnış dəyət daxil edilib");

        }
        switch (numberOfMonth){
            case 1:
                System.out.println("Yanvar");
                break;
            case 2:
                System.out.println("Fevral");
                break;
            case 3:
                System.out.println("Mart");
                break;
            case 4:
                System.out.println("Aprel");
                break;
            case 5:
                System.out.println("May");
                break;
            case 6:
                System.out.println("İyun");
                break;
            case 7:
                System.out.println("İyul");
                break;
            case 8:
                System.out.println("Avqust");
                break;
            case 9:
                System.out.println("Sentyabr");
                break;
            case 10:
                System.out.println("Oktyabr");
                break;
            case 11:
                System.out.println("Noyabr");
                break;
            case 12:
                System.out.println("Dekabr");
                break;

            default:
                System.out.println("Yalnış dəyət daxil edilib");

        }
    }

}
