import java.util.Scanner;

/*
* 17) Scannerden eded girin eyer bu eden ya cutdurse yada 100den
*  boyukdurse "UGURLU"(her ikisinden biri uygundursa). deyilse "UGURSUZ" yazsin
* */
public class Task17 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Tam eded daxil edin: ");
        int num = input.nextInt();

        if (num > 10 || num%2==0) {
            System.out.print("UGURLU");
        } else {
            System.out.print("UGURLSUZ");
        }
    }
}
