import java.util.Scanner;

/*
* 24)Eger menfidirse method geriye -1 qaytarsin, 0-dirsa 0, musbetdirse +1 qaytarsin.
* */
public class Task24 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Reqem daxil edin: ");
        int number = input.nextInt();
        int HandleCheck = checkNumber(number);
        System.out.println(HandleCheck); ;
    }
    public static int checkNumber(int num) {
        if (num > 0) {
            return 1;
        } else if (num < 0) {
            return -1;
        } else {
            return 0;
        }
    }
}
