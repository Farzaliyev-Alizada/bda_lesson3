import java.util.Scanner;
import java.util.SortedMap;

/*
* 11) Verilmish ededin tek ve ya cut olmagini tapin
* */
public class Task11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Reqem daxil edin: ");
        int number = input.nextInt();

        if(number%2==0){
            System.out.println(number + " cut ededdir");
        } else {
            System.out.println(number + " tek ededdir");
        }
    }
}
