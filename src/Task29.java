import java.util.Scanner;

/*
* 29. Method bir String ve bir int qebul edir ve hemin String-in hemin index-inde olan simvolunu qaytarsin.
* */
public class Task29 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("String daxil edin");
        String value = input.nextLine();
        System.out.println("Index nomresini daxil edin: ");
        int index= input.nextInt();
        char indexNumber = CheckIndex(value,index);
        System.out.println("Stringin " + index+"-ci deyeri "+indexNumber);
    }

    public static char CheckIndex(String a, int b){
        return a.charAt(b);
    }
}
