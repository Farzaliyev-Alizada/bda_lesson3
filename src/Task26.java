import java.util.Scanner;

/*
 * 26)methoda daxil edilen reqemin musbet, menfi yoxsa 0 oldugunu teyin eden method yazin.
 * Eger menfidirse method geriye -1 qaytarsin, 0-dirsa 0, musbetdirse +1 qaytarsin.
 * */
public class Task26 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Reqem daxil edin: ");
        int number = input.nextInt();
        int HandleCheck = checkNumber(number);
        System.out.println(HandleCheck); ;
    }
    public static int checkNumber(int num) {
        if (num > 0) {
            return 1;
        } else if (num < 0) {
            return -1;
        } else {
            return 0;
        }
    }
}
