import java.util.Scanner;

/*
* 32. Method 3 String qebul edir. String a, String b, String c. A ve b-nin ichinde c varsa onda true eks halda false
* */
public class Task32 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("1-ci string deyerini daxil edin:");
        String str= input.nextLine();

        System.out.println("2-ci string deyerini daxil edin: ");
        String str2 = input.nextLine();

        System.out.println("3-cu string deyerini daxil edin: ");
        String str3 = input.nextLine();


        boolean result = containsString(str,str2, str3);
        System.out.println(result);
    }
    public static boolean containsString(String a, String b, String c) {
        return a.contains(c) && b.contains(c);
    }
}
