import java.util.Scanner;

/*
* 8) Boolean tipinde deyer gotrun eyer boolean true olsa bashqa yazi deyilse bashqa bir sout verersiz.
* */
public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a boolean type(true/false): ");
        boolean BoolVal= input.nextBoolean();

        if (BoolVal){
            System.out.println("Boolean value is true!");
        } else
                System.out.println("Boolean value is false!");
        }
    }

