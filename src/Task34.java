import java.util.Scanner;

/*
* 34)Bizdən istifadəçi adını və şifrə tələb edən proqram tərtib edin.
*  (Proqramın içində dəyərləri static olaraq təyin edin.)
* Daha sonra username səhv yazıldıqda username yanlışdır. Şifrəni səhv
* yazdıqda şifrə yanlışdır desin. Və şifrəni 3-cü dəfədən artıq yanlış
*  yığarsa sistemdən çıxsın. Əks halda hər biri doğrudursa “Xoş gəldiniz,
*  ‘username’!” – deyə çap etsin. Username hissəsində daxil edilən username yazılsın.
* */
public class Task34 {
    private static  final String Duzgun_Username = "Alizada";
    private static final String Duzgun_Password = "BDA_lab123";

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int cehdSayi = 0;

        while (cehdSayi<3){
            System.out.println("Username daxil edin: ");
            String username = input.nextLine();

            System.out.println("Parol daxil edin: ");
            String password = input.nextLine();

            if(username.contains(Duzgun_Username) && password.contains(Duzgun_Password)) {
                System.out.println("Xoş gəldiniz, " + username +"!");
            } else if (username.contains(Duzgun_Username)) {
                System.out.println("Shifre yalnishdir, tekrar daxil edin");
            } else {
                System.out.println("Username yalnishdir, tekrar daxil edin");
            }
            cehdSayi++;
        }
        System.out.println("Maksimum 3 defe yalnish melumat daxil edile biler");
    }

}
