import java.util.Scanner;

/*
* 16). Scannerden eded girin ve bu eded hem cut eded hem 50 den boyuk olsa
* "Ugurlu emelyat" bu ikisinden birini odemirse "UGURSUZ emelyat" yazsin;
* */
public class Task16 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Tam eded daxil edin: ");
        int num = input.nextInt();

        if (num > 50 && num%2==0) {
            System.out.print("Ugurlu emeliyyat");
        } else {
            System.out.print("Ugursuz emeliyyat");
        }
    }
}
