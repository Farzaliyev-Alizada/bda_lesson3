import java.util.Scanner;

/*
* 2)10luq say sisteminde daxil edilmish ededi 2lik koda cheviren program yazin.
* */
public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a decimal number: ");
        int decimalNum = input.nextInt();
        String binaryNum = Integer.toBinaryString(decimalNum);
        System.out.println(binaryNum);
    }
}
