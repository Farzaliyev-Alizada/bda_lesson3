import java.time.Duration;
import java.util.Scanner;

/*
* Switch if-dən daha sürətlidir, çünki if-else ifadələri üzərində dövr edərkən hər bir
* ifadənin doğru olub olmadığını yoxlamaq üçün müqayisə etməlidir.Böyük datalarla çalışarkən, zaman itkisi hiss olunur.

Switch sətiri isə bir dəyərin mövcudluğunu yoxlamaq üçün "case" blokları ilə işləyir.
*  Buna sərf edilən zaman daha azdır və daha sürətli işləyir.
*
* 7)Biz deyirik ki switch if-den daha suretlidir. Bu ne ucun beledir? Bunu kod numensi yazaraq izah edin
(mumkun olsa suret vaxtini qeyd edin google search edib  chox maraqlidir)

*
* */public class Task7 {
    public static void main(String[] args) {

        int num;
        Scanner input = new Scanner(System.in);
        System.out.println("Rəqəm daxil edin: ");
        num=input.nextInt();
        long start,duration,end;

        start=System.nanoTime();
// Switchin ishlediyi vaxti teyin edek
        switch (num){
            case 1:
                System.out.println("bir");
            break;
            case 2:
                System.out.println("iki");
                break;
            case 3:
                System.out.println("üç");
                break;
            case 4:
                System.out.println("dörd");
                break;
            case 5:
                System.out.println("beş");
                break;
            default:
                System.out.println("Beş və ya daha kiçik rəqəm daxil edin");

        }
        end = System.nanoTime();
        duration=end-start;
        System.out.println("Switch= "+duration );


        // if-else vaxtini teyin edek

        start=System.nanoTime();
        if(num==1){
            System.out.println("bir");
        } else if (num==2){
            System.out.println("iki");
        } else if (num==3){
            System.out.println("üç");
        } else if (num==4){
            System.out.println("dörd");
        } else if (num==5){
            System.out.println("beş");
        } else {
            System.out.println("Beş və ya daha kiçik rəqəm daxil edin");
        }

        end = System.nanoTime();
        duration=end-start;
        System.out.println("if/else= "+duration );
    }
}
