/*
* 4)10lug say sisteminde daxil edilmish ededi 8liye cevirin
* */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input= new Scanner(System.in);
        System.out.println("Please enter a decimal number: ");
        int decimal = input.nextInt();
        String octalString = Integer.toOctalString(decimal);
        System.out.println(octalString);
    }
}
