import java.util.Scanner;

/*
* 27. Methoda 4 reqem daxil edilir eger bu reqemlerden her hansisa 2-si bir birine beraberdirse geriye true qaytarsin
* */
public class Task27 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num1,num2,num3,num4;
        System.out.println("Birinci reqemi daxil edin: ");
        num1=input.nextInt();
        System.out.println("Ikinci reqemi daxil edin: ");
        num2=input.nextInt();
        System.out.println("Uchuncu reqemi daxil edin: ");
        num3=input.nextInt();
        System.out.println("Dorduncu reqemi daxil edin: ");
        num4=input.nextInt();
        boolean HandleCheck = checkNumber(num1,num2,num3,num4);
        System.out.println(HandleCheck); ;
    }


    public static boolean checkNumber(int a ,int b,int c, int d) {

        if (a ==b || a ==c || a ==d || b ==c || b ==d) {
            return true;
        }else {
            System.out.println("Reqemler beraber deyil");
        }
        return false;
    }
}
